import com.optimaize.linguist.scribe.Converter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HiraganaToRomajiTest {


    private final Converter converter = Converter.getInstance();

    @Test(dataProvider = "hiraganaToRomaji")
    public void hiraganaToRomaji(String input, String expected) {

        String latin = converter.kanaToLatin(input);
        assertEquals(latin, expected);

    }

    @DataProvider
    public Object[][] hiraganaToRomaji() {
        return new Object[][]{
                {"むかいやま", "Mukaiyama"},
                {"い, う", "I, U"},
                {"たかとう", "Takatoo"},
                {"いづみ", "Izumi"},
                {"いずみ", "Izumi"},
                {"たかとお", "Takatoo"},
                {"おの", "Ono"},
                {"おうの", "Ono, Ohno, Oono"},
                {"おおや", "Oya, Ohya, Ooya"},
                {"おおの", "Ono, Ohno, Oono"},
                {"ぎゃ", "Gya"},
                {"ちゅ", "Chu"},
                {"ちゅう", "Chu"},
                {"ぎゅう", "Gyu"},
                {"すずき", "Suzuki"},
                {"やまぐち", "Yamaguchi"},
                {"やまくち", "Yamakuchi"},
                {"うえだ", "Ueda"},
                {"うえでん", "Ueden"},
                {"ようだ", "Yoda, Yohda, Yooda"},
                {"ごどう", "Godoo"},
                {"かどわき", "Kadowaki"},
                {"どうした", "Doshita, Dohshita, Dooshita"},

                {"ぎょと", "Gyoto"},
                {"きょと", "Kyoto"},
                {"みょと", "Myoto"},
                {"しょと", "Shoto"},
                {"じょと", "Joto"},
                {"にょと", "Nyoto"},
                {"りょと", "Ryoto"},
                {"ひおと", "Hioto"},
                {"ひょと", "Hyoto"},
                {"におと", "Nioto"},
                {"ちょと", "Choto"},

                {"ぎょうと", "Gyoto, Gyohto, Gyooto"},
                {"きょうと", "Kyoto, Kyohto, Kyooto"},
                {"みょうと", "Myoto, Myohto, Myooto"},
                {"しょうと", "Shoto, Shohto, Shooto"},
                {"じょうと", "Joto, Johto, Jooto"},
                {"にょうと", "Nyoto, Nyohto, Nyooto"},
                {"りょうと", "Ryoto, Ryohto, Ryooto"},
                {"ちょうと", "Choto, Chohto, Chooto"}
        };
    }

}
