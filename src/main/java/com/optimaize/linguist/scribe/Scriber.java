package com.optimaize.linguist.scribe;

import java.text.Normalizer;

/**
 * Provides methods to parse, handle and verify Kana, Kanji and Latin scripts.
 */
public class Scriber {

    private static final int HIRAGANA_START = 0x3040;
    private static final int HIRAGANA_END = 0x309F;
    private static final int KATAKANA_START = 0x30A0;
    private static final int KATAKANA_END = 0x30FF;
    private static final int KANJI_START = 0x4E00; //common and uncommon kanji
    private static final int KANJI_END = 0x9FAF; //common and uncommon kanji
    private static final int RARE_KANJI_START = 0x3400;
    private static final int RARE_KANJI_END = 0x4DBF;
    private static final int JAPANESE_PUNCTUATION_START = 0x3000;
    private static final int JAPANESE_PUNCTUATION_END = 0x303F;

    public Scriber() {
    }

    /**
     * @return Only the Hiragana characters from the string.
     */
    public static String getHiragana(String s) {
        return s.replace("[^ぁ-ん]","");
    }

    /**
     * @return Only the Katakana characters from the string.
     */
    public static String getKatakana(String s) {
        return s.replace("[^゠-ヿ]","");
    }

    /**
     * Checks if a character is from the Katakana script.
     */
    public boolean isCharKatakana(char chr) {
        return isCharInRange(chr, KATAKANA_START, KATAKANA_END);
    }

    /**
     * Checks if a character is from the Hiragana script.
     */
    public boolean isCharHiragana(char chr) {
        return isCharInRange(chr, HIRAGANA_START, HIRAGANA_END);
    }

    /**
     * /**
     * Checks if a character is a Japanese punctuation.
     */
    private boolean isCharJapanesePunctuation(char chr) {
        return isCharInRange(chr, JAPANESE_PUNCTUATION_START, JAPANESE_PUNCTUATION_END);
    }

    /**
     * Checks if a character is a kana syllable (Hiragana or Katakana).
     */
    public boolean isCharKana(char chr) {
        return isCharHiragana(chr) || isCharKatakana(chr);
    }

    /**
     * Checks if a character is a Japanese Kanji character.
     */
    public boolean isCharKanji(char chr) {
        if (!isCharInRange(chr, KANJI_START, KANJI_END)) {
            if (!isCharInRange(chr, RARE_KANJI_START, RARE_KANJI_END)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the character is a certain Unicode range.
     *
     * Example: If it's between 0x3040 and 0x309F, then it's a Hiragana character.
     * @param chr character to verify
     * @param start from which index to start checking
     * @param end until which index to check
     */
    private boolean isCharInRange(char chr, int start, int end) {
        int unicode = chr;
        return (start <= unicode && unicode <= end);
    }

    /**
     * Checks if an entire string contains only Hiragana characters.
     */
    public boolean isHiragana(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!isCharHiragana(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if an entire string contains only Hiragana characters.
     */
    public boolean isKatakana(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!isCharKatakana(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if an entire string contains only Kanji characters.
     */
    public boolean isKanji(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!isCharKanji(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if an entire string contains only Japanese punctuations.
     */
    public boolean isJapanesePunctuation(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!isCharJapanesePunctuation(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if a string is ASCII.
     */
    public boolean isAscii(String s) {
        return s.chars().allMatch(c -> c < 128);
    }

    /**
     * Normalizes ASCII characters.
     *
     * Example: ȩ -> e; ș -> s etc.
     * @return The normalized string, or an empty string if it's not normalizable.
     */
    public String normalizeAscii(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        return s.replaceAll("[^\\p{ASCII}]", "");
    }

    /**
     * Checks if a string is Latin, by normalizing the ASCII first.
     */
    public boolean isLatin(String s) {
        return normalizeAscii(s).matches("[A-Za-z]+");
    }

}
