package com.optimaize.linguist.scribe;

import org.apache.commons.text.WordUtils;

import java.util.HashMap;

/**
 * Provides conversion methods for Katakana, Hiragana and Latin scripts.
 * <p>
 * Syllables ending in 'o' followed by 'u', the 'u' is removed.
 * Syllables ending in 'i' followed by 'half-y', followed by 'u', the 'u' is removed.
 */
public class Converter {

    private static final int HIRAGANA_START = 0x3040;
    private static final int HIRAGANA_END = 0x309F;
    private static final int KATAKANA_START = 0x30A0;
    private static final int KATAKANA_END = 0x30FF;

    private HashMap<String, String> mJtoL = new HashMap<>();
    private Scriber scriber = new Scriber();

    private Converter() {
        prepareJtoL();
    }

    private static final Converter INSTANCE = new Converter();

    public static Converter getInstance() {
        return Converter.INSTANCE;
    }

    /**
     * Coverts Katakana script to Hiragana script
     */
    public String katakanaToHiragana(String kata) {
        int unicode;
        StringBuilder hiragana = new StringBuilder();

        for (int i = 0; i < kata.length(); i++) {
            char katakanaChar = kata.charAt(i);

            if (scriber.isCharKatakana(katakanaChar)) {
                unicode = katakanaChar;
                unicode += HIRAGANA_START - KATAKANA_START;
                hiragana.append(String.valueOf(Character.toChars(unicode)));
            } else {
                hiragana.append(katakanaChar);
            }
        }

        return hiragana.toString();
    }

    /**
     * Converts Hiragana script to Katakana script
     */
    public String hiraganaToKatakana(String hiragana) {
        int unicode;
        StringBuilder katakana = new StringBuilder();

        for (int i = 0; i < hiragana.length(); i++) {
            char hiraganaChar = hiragana.charAt(i);

            if (scriber.isCharHiragana(hiraganaChar)) {
                unicode = hiraganaChar;
                unicode += KATAKANA_START - HIRAGANA_START;
                katakana.append(String.valueOf(Character.toChars(unicode)));
            } else {
                katakana.append(hiraganaChar);
            }
        }

        return katakana.toString();
    }

    /**
     * Converts Kana syllables (Hiragana and Katakana) to Latin script.
     */
    public String kanaToLatin(String kana) {
        //if it's already latin, just return the value
        if (scriber.isLatin(kana)) {
            return kana;
        }

        String chunk = ""; //chars to work with
        int chunkSize; //size of chars to work with
        int cursor = 0; //used to iterate through each character
        int len = kana.length();
        int maxChunk = 2; //maximum size of chars to work with
        boolean nextCharIsDoubleConsonant = false;
        StringBuilder latin = new StringBuilder();
        String latinChar = null;

        //Iterate through the entire word, 2 hiragana chars at a time, and then 1 at a time
        while (cursor < len) {
            //can be max 2 characters at once; Math.min in case there is 1 character
            chunkSize = Math.min(maxChunk, len - cursor);
            while (chunkSize > 0) {
                chunk = kana.substring(cursor, (cursor + chunkSize)).trim(); //cut from cursor position up until the char to work with

                //in case there is katakana in the text, by mistake, translate it
                if (scriber.isKatakana(chunk)) {
                    chunk = katakanaToHiragana(chunk);
                }

                //detect double consonant
                if (chunkSize == 1 & String.valueOf(chunk.charAt(0)).equals("っ") & cursor < (len - 1)) {
                    nextCharIsDoubleConsonant = true;
                    latinChar = "";
                    break;
                }

                //detect diphthong as last position
                if (chunk.contains("う") & kana.indexOf("う") == kana.length() - 1) {
                    String s = mJtoL.get(String.valueOf(chunk.charAt(0)));
                    if (chunk.length() > 1) {
                        if (s.length() == 1) {
                            s = s.concat(s);
                        } else {
                            try {
                                s = s.concat(String.valueOf(s.charAt(1)));
                            } catch (StringIndexOutOfBoundsException e) {
                                System.out.println(e);
                            }
                        }
                    }
                    latinChar = s;
                } else if (chunk.contains("う") & kana.indexOf("う") != kana.length() - 1 & mJtoL.get(chunk) == null & kana.indexOf("う") != 0) {
                    if (latin.toString().endsWith("o")) {
                        latinChar = mJtoL.get(chunk.substring(1, 2));
                        latin = latin.replace(latin.indexOf("o"), latin.indexOf("o") + 1, "ō");

                    } else {
                        String s = mJtoL.get(String.valueOf(chunk.charAt(0)));
                        latinChar = s;
                    }
                } else {
                    //get the latin translation for the chunk
                    latinChar = mJtoL.get(chunk);
                }


                //add the double consonant if detected
                if ((latinChar != null) && nextCharIsDoubleConsonant) {
                    latinChar = latinChar.charAt(0) + latinChar;
                    nextCharIsDoubleConsonant = false;
                }

                //if chars were translated, preserve chunk size
                if (latinChar != null) {
                    break;
                }

                //if chars were not translated, decrease chunk size
                chunkSize--;
            }

            //if char was not translated at all, add it; it's latin or punctuation
            if (latinChar == null) {
                latinChar = chunk;
            }

            latin.append(latinChar);
            //increase cursor position
            cursor += chunkSize > 0 ? chunkSize : 1;
        }
        String result = latin.toString();
        if (result.contains("ō") & result.indexOf("ō") != result.length() - 1) {//hack, works.
            String first = result.replace("ō", "o");
            String second = result.replace("ō", "oh");
            String third = result.replace("ō", "oo");
            result = String.join(",", first, second, third);
        }
        if (result.endsWith("uu")) {
            result = result.replace("uu", "u");
        }
        return WordUtils.capitalizeFully(result.replace(",", ", "));
    }

    /**
     * Prepares the entire hiragana to latin map, for later use.
     */
    private void prepareJtoL() {
        mJtoL.put("あ", "a");
        mJtoL.put("い", "i");
        mJtoL.put("う", "u");
        mJtoL.put("え", "e");
        mJtoL.put("お", "o");
        mJtoL.put("おお", "ō"); //like this so I can make the variants at the end
        mJtoL.put("おう", "ō"); //like this so I can make the variants at the end
        mJtoL.put("こう", "kō");
        mJtoL.put("ごう", "gō");
        mJtoL.put("そう", "sō");
        mJtoL.put("ぞう", "zō");
        mJtoL.put("とう", "tō");
        mJtoL.put("どう", "dō");
        mJtoL.put("のう", "nō");
        mJtoL.put("ほう", "hō");
        mJtoL.put("ぼう", "bō");
        mJtoL.put("ぽう", "pō");
        mJtoL.put("もう", "mō");
        mJtoL.put("よう", "yō");
        mJtoL.put("ろう", "rō");
        mJtoL.put("をう", "wō");
        mJtoL.put("ぉう", "ō");
        mJtoL.put("ょう", "yō");

        mJtoL.put("ゔぁ", "va");
        mJtoL.put("ゔぃ", "vi");
        mJtoL.put("ゔ", "vu");
        mJtoL.put("ゔぇ", "ve");
        mJtoL.put("ゔぉ", "vo");
        mJtoL.put("か", "ka");
        mJtoL.put("き", "ki");
        mJtoL.put("きゃ", "kya");
        mJtoL.put("きぃ", "kyi");
        mJtoL.put("きゅ", "kyu");
        mJtoL.put("く", "ku");
        mJtoL.put("け", "ke");
        mJtoL.put("こ", "ko");
        mJtoL.put("が", "ga");
        mJtoL.put("ぎ", "gi");
        mJtoL.put("ぐ", "gu");
        mJtoL.put("げ", "ge");
        mJtoL.put("ご", "go");
        mJtoL.put("ぎゃ", "gya");
        mJtoL.put("ぎぃ", "gyi");
        mJtoL.put("ぎゅ", "gyu");
        mJtoL.put("ぎぇ", "gye");
        mJtoL.put("ぎょ", "gyo");
        mJtoL.put("さ", "sa");
        mJtoL.put("す", "su");
        mJtoL.put("せ", "se");
        mJtoL.put("そ", "so");
        mJtoL.put("ざ", "za");
        mJtoL.put("ず", "zu");
        mJtoL.put("ぜ", "ze");
        mJtoL.put("ぞ", "zo");
        mJtoL.put("し", "shi");
        mJtoL.put("しゃ", "sha");
        mJtoL.put("しゅ", "shu");
        mJtoL.put("しょ", "sho");
        mJtoL.put("じ", "ji");
        mJtoL.put("じゃ", "ja");
        mJtoL.put("じゅ", "ju");
        mJtoL.put("じょ", "jo");
        mJtoL.put("た", "ta");
        mJtoL.put("ち", "chi");
        mJtoL.put("ちゃ", "cha");
        mJtoL.put("ちゅ", "chu");
        mJtoL.put("ちょ", "cho");
        mJtoL.put("つ", "tsu");
        mJtoL.put("て", "te");
        mJtoL.put("と", "to");
        mJtoL.put("だ", "da");
        mJtoL.put("ぢ", "ji");
        mJtoL.put("づ", "zu");
        mJtoL.put("で", "de");
        mJtoL.put("ど", "do");
        mJtoL.put("な", "na");
        mJtoL.put("に", "ni");
        mJtoL.put("にゃ", "nya");
        mJtoL.put("にゅ", "nyu");
        mJtoL.put("にょ", "nyo");
        mJtoL.put("ぬ", "nu");
        mJtoL.put("ね", "ne");
        mJtoL.put("の", "no");
        mJtoL.put("は", "ha");
        mJtoL.put("ひ", "hi");
        mJtoL.put("ふ", "fu");
        mJtoL.put("へ", "he");
        mJtoL.put("ほ", "ho");
        mJtoL.put("ひゃ", "hya");
        mJtoL.put("ひゅ", "hyu");
        mJtoL.put("ひょ", "hyo");
        mJtoL.put("ふぁ", "fa");
        mJtoL.put("ふぃ", "fi");
        mJtoL.put("ふぇ", "fe");
        mJtoL.put("ふぉ", "fo");
        mJtoL.put("ば", "ba");
        mJtoL.put("び", "bi");
        mJtoL.put("ぶ", "bu");
        mJtoL.put("べ", "be");
        mJtoL.put("ぼ", "bo");
        mJtoL.put("びゃ", "bya");
        mJtoL.put("びゅ", "byu");
        mJtoL.put("びょ", "byo");
        mJtoL.put("ぱ", "pa");
        mJtoL.put("ぴ", "pi");
        mJtoL.put("ぷ", "pu");
        mJtoL.put("ぺ", "pe");
        mJtoL.put("ぽ", "po");
        mJtoL.put("ぴゃ", "pya");
        mJtoL.put("ぴゅ", "pyu");
        mJtoL.put("ぴょ", "pyo");
        mJtoL.put("ま", "ma");
        mJtoL.put("み", "mi");
        mJtoL.put("む", "mu");
        mJtoL.put("め", "me");
        mJtoL.put("も", "mo");
        mJtoL.put("みゃ", "mya");
        mJtoL.put("みゅ", "myu");
        mJtoL.put("みょ", "myo");
        mJtoL.put("や", "ya");
        mJtoL.put("ゆ", "yu");
        mJtoL.put("よ", "yo");
        mJtoL.put("ら", "ra");
        mJtoL.put("り", "ri");
        mJtoL.put("る", "ru");
        mJtoL.put("れ", "re");
        mJtoL.put("ろ", "ro");
        mJtoL.put("りゃ", "rya");
        mJtoL.put("りゅ", "ryu");
        mJtoL.put("りょ", "ryo");
        mJtoL.put("わ", "wa");
        mJtoL.put("を", "wo");
        mJtoL.put("ん", "n");
        mJtoL.put("ゐ", "wi");
        mJtoL.put("ゑ", "we");
        mJtoL.put("きぇ", "kye");
        mJtoL.put("きょ", "kyo");
        mJtoL.put("じぃ", "jyi");
        mJtoL.put("じぇ", "jye");
        mJtoL.put("ちぃ", "cyi");
        mJtoL.put("ちぇ", "che");
        mJtoL.put("ひぃ", "hyi");
        mJtoL.put("ひぇ", "hye");
        mJtoL.put("びぃ", "byi");
        mJtoL.put("びぇ", "bye");
        mJtoL.put("ぴぃ", "pyi");
        mJtoL.put("ぴぇ", "pye");
        mJtoL.put("みぇ", "mye");
        mJtoL.put("みぃ", "myi");
        mJtoL.put("りぃ", "ryi");
        mJtoL.put("りぇ", "rye");
        mJtoL.put("にぃ", "nyi");
        mJtoL.put("にぇ", "nye");
        mJtoL.put("しぃ", "syi");
        mJtoL.put("しぇ", "she");
        mJtoL.put("いぇ", "ye");
        mJtoL.put("うぁ", "wha");
        mJtoL.put("うぉ", "who");
        mJtoL.put("うぃ", "wi");
        mJtoL.put("うぇ", "we");
        mJtoL.put("ゔゃ", "vya");
        mJtoL.put("ゔゅ", "vyu");
        mJtoL.put("ゔょ", "vyo");
        mJtoL.put("すぁ", "swa");
        mJtoL.put("すぃ", "swi");
        mJtoL.put("すぅ", "swu");
        mJtoL.put("すぇ", "swe");
        mJtoL.put("すぉ", "swo");
        mJtoL.put("くゃ", "qya");
        mJtoL.put("くゅ", "qyu");
        mJtoL.put("くょ", "qyo");
        mJtoL.put("くぁ", "qwa");
        mJtoL.put("くぃ", "qwi");
        mJtoL.put("くぅ", "qwu");
        mJtoL.put("くぇ", "qwe");
        mJtoL.put("くぉ", "qwo");
        mJtoL.put("ぐぁ", "gwa");
        mJtoL.put("ぐぃ", "gwi");
        mJtoL.put("ぐぅ", "gwu");
        mJtoL.put("ぐぇ", "gwe");
        mJtoL.put("ぐぉ", "gwo");
        mJtoL.put("つぁ", "tsa");
        mJtoL.put("つぃ", "tsi");
        mJtoL.put("つぇ", "tse");
        mJtoL.put("つぉ", "tso");
        mJtoL.put("てゃ", "tha");
        mJtoL.put("てぃ", "thi");
        mJtoL.put("てゅ", "thu");
        mJtoL.put("てぇ", "the");
        mJtoL.put("てょ", "tho");
        mJtoL.put("とぁ", "twa");
        mJtoL.put("とぃ", "twi");
        mJtoL.put("とぅ", "twu");
        mJtoL.put("とぇ", "twe");
        mJtoL.put("とぉ", "two");
        mJtoL.put("ぢゃ", "dya");
        mJtoL.put("ぢぃ", "dyi");
        mJtoL.put("ぢゅ", "dyu");
        mJtoL.put("ぢぇ", "dye");
        mJtoL.put("ぢょ", "dyo");
        mJtoL.put("でゃ", "dha");
        mJtoL.put("でぃ", "dhi");
        mJtoL.put("でゅ", "dhu");
        mJtoL.put("でぇ", "dhe");
        mJtoL.put("でょ", "dho");
        mJtoL.put("どぁ", "dwa");
        mJtoL.put("どぃ", "dwi");
        mJtoL.put("どぅ", "dwu");
        mJtoL.put("どぇ", "dwe");
        mJtoL.put("どぉ", "dwo");
        mJtoL.put("ふぅ", "fwu");
        mJtoL.put("ふゃ", "fya");
        mJtoL.put("ふゅ", "fyu");
        mJtoL.put("ふょ", "fyo");
        mJtoL.put("ぁ", "a");
        mJtoL.put("ぃ", "i");
        mJtoL.put("ぇ", "e");
        mJtoL.put("ぅ", "u");
        mJtoL.put("ぉ", "o");
        mJtoL.put("ゃ", "ya");
        mJtoL.put("ゅ", "yu");
        mJtoL.put("ょ", "yo");
        mJtoL.put("っ", "tsu");
        mJtoL.put("ゕ", "ka");
        mJtoL.put("ゖ", "ka");
        mJtoL.put("ゎ", "wa");
        mJtoL.put("'　'", " ");
        mJtoL.put("んあ", "na");
        mJtoL.put("んい", "ni");
        mJtoL.put("んう", "nu");
        mJtoL.put("んえ", "ne");
        mJtoL.put("んお", "no");
        mJtoL.put("んや", "nya");
        mJtoL.put("んゆ", "nyu");
        mJtoL.put("んよ", "nyo");
    }

}