package com.optimaize.linguist.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Provides methods to write the output to .txt files.
 */
public class Writer {

    private static final String BASE_PATH = System.getProperty("user.dir");
    private static final File RESOURCE_FOLDER = new File(BASE_PATH + "/src/main/resources/");

    public Writer() {
    }

    /**
     * Writes a string to an output.txt file, each string on a separate line.
     */
    public static void writeToOutput(String s) {
        try {
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(new File(RESOURCE_FOLDER + "/output.txt"), true));
            bw.append(s).append("\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
