package com.optimaize.linguist;

import com.optimaize.linguist.scribe.Converter;
import com.optimaize.linguist.tools.Writer;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 */
public class App {

    private static final String BASE_PATH = System.getProperty("user.dir");
    private static final File RESOURCE_FOLDER = new File(BASE_PATH + "/src/main/resources/");

    public static void main(String[] args) {

        Converter converter = Converter.getInstance();
        File input = new File(RESOURCE_FOLDER + "/input.txt");

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));

            String hiragana;
            while ((hiragana = br.readLine()) != null) {
                String test = converter.kanaToLatin(hiragana);
                Writer.writeToOutput(test);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}